﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [System.Serializable]
    public class LevelData
    {
        public string Name;
        public GameObject LevelObject;
        public int EnemysForWin;
        public int MoneyReward;
        public int TimeForLevel;
    }

    public static int curLevel = 0; // Для переключения уровней

    public LevelData[] levels;

    void Start()
    {
        for(int i = 0; i < levels.Length; i++ )
        {
            levels[i].LevelObject.SetActive ( i == curLevel );
        }
    }
}
