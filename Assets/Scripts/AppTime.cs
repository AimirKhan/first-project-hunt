﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SimpleJSON;
using System;

public class AppTime : MonoBehaviour
{
    //"https://yandex.com/time/sync.json?geo=213"
    const string time_url = "http://now.httpbin.org/";
    //"https://script.googleusercontent.com/macros/echo?user_content_key=mikXMEgFo-tAQuWutA5G97y0PulyNQsc6qkwTd4ELTCgU-qpYuTfV1j8R9zbPjFeGBRma7HeqVR9UsKM0ZqSjX2x8g_pDZcom5_BxDlH2jW0nuo2oDemN9CCS2h10ox_1xSncGQajx_ryfhECjZEnJ9GRkcRevgjTvo8Dc32iw_BLJPcPfRdVKhJT5HNzQuXEeN3QFwl2n0M6ZmO-h7C6eIqWsDnSrEd&lib=MwxUjRcLr2qLlnVOLh12wSNkqcO1Ikdrk"

    public static DateTime CurrentTime;
    public static AppTime Instance;

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad ( gameObject );
            Instance = this;
        }
        else
        {
            Destroy ( gameObject );
        }
    }

    IEnumerator TimeTick()
    {
        yield return new WaitForSeconds ( 1 );
        CurrentTime = CurrentTime.AddSeconds ( 1 );
        StartCoroutine ( TimeTick() );
    }

    public IEnumerator GetServerTime(Action<bool> result)
    {
        if ( default(DateTime) == CurrentTime )
        {
            using ( UnityWebRequest www = UnityWebRequest.Get ( time_url ) )
            {
                www.timeout = 5;
                yield return www.SendWebRequest ();

                if ( string.IsNullOrEmpty ( www.error ) )
                {
                    // okay, no errors
                    string wwwString = www.downloadHandler.text;
                    JSONNode node = JSON.Parse ( wwwString );
                    CurrentTime = new DateTime ();
                    if ( DateTime.TryParse ( node["now"]["rfc2822"], out CurrentTime ) )
                    {
                        // Success
                    }
                    else
                    {
                        // Not Date
                        Debug.LogError ( node["now"]["rfc2822"] + " Not Date!" );
                    }

                    result ( true );
                }
                else
                {
                    // error, something need to do
                    result ( false );
                }
                Debug.Log ( "saf" );
            }
        }
        else
        {
            result ( true );
            yield return null;
        }
    }
}
