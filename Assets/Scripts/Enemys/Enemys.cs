﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemys : MonoBehaviour
{
    public int health;
    public bool run;

    Animator anim;

    NavMeshAgent NMA;
    Transform player;
    bool runToPlayer;

    float attackTime = 1;
    float curAttackTime = 0;

    void Awake()
    {
        anim = GetComponent<Animator> ();
        NMA = GetComponent<NavMeshAgent> ();
        player = GameObject.FindGameObjectWithTag ( "Player" ).transform;

        Weapon.onWeaponShoot += EnemysRun;

        Cheat.onKillAll += Damage;
    }

    void OnDestroy()
    {
        Cheat.onKillAll -= Damage;
        Weapon.onWeaponShoot -= EnemysRun;
    }

    void Update()
    {
        if (NMA.remainingDistance < NMA.stoppingDistance + .5f && health > 0 && NMA.hasPath)
        {
            run = false;
            anim.SetBool ( "run", run );
            if( runToPlayer )
            {
                Attack ();
            }
        }
    }

    public void Attack()
    {
        if ( curAttackTime >= attackTime )
        {
            anim.SetTrigger ( "attack" );
            // Нанёс урон игроку
            if ( CameraEffects.playerDamaged != null )
            {
                CameraEffects.playerDamaged ();
            }
            curAttackTime = 0;
        }
        else
        {
            curAttackTime += Time.deltaTime;
        }
    }

    public virtual void EnemysRun()
    {

    }

    public void Escape ()
    {
        if ( health > 0 )
        {
            run = true;
            NMA.SetDestination ( new Vector3 ( Random.Range ( -10, 10 ), 0, Random.Range ( -10, 10 ) ) );
            anim.SetBool ( "run", run );
        }
    }

    public void RunToPlayer()
    {
        run = true;
        NMA.SetDestination ( player.position );
        anim.SetBool ( "run", run );
        runToPlayer = true;
    }

    public void Damage (int damage)
    {
        
        health -= damage;
        if(health <=0)
        {
            Weapon.onWeaponShoot -= EnemysRun;
            NMA.ResetPath ();
            anim.SetBool ( "death", true );
            anim.SetBool ( "run", false );

            if ( health <= 0 )
            {
                if ( TutorialController.currentPhase == 2 )
                {
                    if ( TutorialController.changePhaseAction != null )
                    {
                        TutorialController.changePhaseAction ( 3 );
                    }
                }
            }
        }
        
    }
}
