﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeWeapon : MonoBehaviour
{
    public GameObject[] allWeapons;
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            OnPistolActive ();
        }
        if ( Input.GetKeyDown ( KeyCode.Alpha2 ) )
        {
            OnGrenadeActive ();
        }
    }

    void RefreshVisibleWeapon(int weaponIndex)
    {
        for (int i = 0; i < allWeapons.Length; i++ )
        {
            allWeapons[i].SetActive ( i == weaponIndex );
        }
    }

    public void OnGrenadeActive()
    {
        if ( GlobalStats.HasGrenade )
        {
            RefreshVisibleWeapon ( 1 );
        }
    }
    public void OnPistolActive()
    {
        RefreshVisibleWeapon (0);
    }
}
