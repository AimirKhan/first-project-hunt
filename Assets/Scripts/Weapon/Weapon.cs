﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Weapon : MonoBehaviour
{
    public int bullet;
    public int damage;
    public string soundName;
    public static Action onWeaponShoot;
    Animator anim;

    public virtual void OnPlayerShoot()
    {

    }

    void OnDisable()
    {
        UIController.WeaponShoot -= OnPlayerShoot;
    }

    void Start()
    {
        
        anim = GetComponent<Animator> ();
        if ( UIController.OnShoot != null )
        {
            UIController.OnShoot ( bullet );
        }
    }


    void OnEnable()
    {
        UIController.WeaponShoot += OnPlayerShoot;
    }
    public void OnStartShoot()
    {
            if ( TutorialController.currentPhase == 1 )
            {
                if ( TutorialController.changePhaseAction != null )
                {
                    TutorialController.changePhaseAction ( 2 );
                }
            }

        bullet--;
        anim.SetTrigger ( "shoot" );
        SoundPlayer.instance.Play ( soundName, 1 );
        if ( UIController.OnShoot != null )
        {
            UIController.OnShoot ( bullet );
        }

        if ( onWeaponShoot != null )
        {
            onWeaponShoot ();
        }
    }
}
