﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    public GameObject HitParticles;
    public ParticleSystem Muzzle;
    RaycastHit hit;
    public LineRenderer superBulletEffect;

    void Awake()
    {
        superBulletEffect.enabled = false;
    }

    public override void OnPlayerShoot()
    {
        base.OnPlayerShoot ();
        if ( bullet > 0 && Time.timeScale != 0 )
        {
            Muzzle.Play ( true );

            OnStartShoot ();

            UIController.OnShoot ( bullet );
            Ray ray = Camera.main.ScreenPointToRay ( new Vector3 ( Screen.width / 2, Screen.height / 2, 0 ) );

            if ( Physics.Raycast ( ray.origin, ray.direction * 10000, out hit ) )
            {
                GameObject hitEffect = ( GameObject ) Instantiate ( HitParticles, hit.point, Quaternion.identity );
                Destroy ( hitEffect, 2 ); // Удаление обеьктов для незамусоривания памяти

                Enemys enemies;
                if ( ( enemies = hit.collider.GetComponent<Enemys> () ) != null )
                {
                    if(Random.Range(0f,100f)< GlobalStats.SuperBulletEffect)
                    {
                        enemies.Damage ( 999999 );
                        StartCoroutine ( SuperBulletEffect_Shoot (hit.point) );
                    }
                    else
                    {
                        enemies.Damage ( damage );
                    }
                    enemies.Damage ( damage );
                }
            }
        }
        if ( Input.GetKeyDown ( KeyCode.R ) )
        {
            bullet = 10;
        }
    }

    IEnumerator SuperBulletEffect_Shoot (Vector3 endPosition)
    {
        superBulletEffect.enabled = true;
        superBulletEffect.SetPosition ( 1, endPosition );
        yield return new WaitForSeconds ( .1f );
        superBulletEffect.enabled = false;
    }
}
