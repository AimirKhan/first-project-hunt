﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : Weapon
{
    
    public Transform shootPoint;
    public GameObject grenadeObject;
    public GameObject explosionsEffect;
    int force = 1000;
    int timer = 2;
    int damageRadius = 10;

    public override void OnPlayerShoot()
    {
        base.OnPlayerShoot ();
        if ( bullet > 0 && Time.timeScale != 0 )
        {
            OnStartShoot (); // Звук эффекты патроны

            GameObject newGrenade = (GameObject)Instantiate ( grenadeObject, shootPoint.position, Quaternion.identity );
            newGrenade.GetComponent<Rigidbody> ().AddForce (shootPoint.forward * force);

            StartCoroutine ( Explosions ( newGrenade.transform ) );
        }
    }

    IEnumerator Explosions(Transform grenadeTransform)
    {
        yield return new WaitForSeconds ( timer );
        Instantiate ( explosionsEffect, grenadeTransform.position, Quaternion.identity );
        //RaycastHit hit;
        Collider[] colls = Physics.OverlapSphere ( grenadeTransform.position, damageRadius );
        Enemys enemy;
        for(int i = 0; i <colls.Length; i++ )
        {
            if((enemy = colls[i].GetComponent<Enemys>()) != null)
            {
                enemy.Damage ( damage );
            }
        }
        SoundPlayer.instance.Play ( "grenade", 1 );
        Destroy ( grenadeTransform.gameObject );
    }
}
