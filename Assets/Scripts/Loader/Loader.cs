﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    public Image loaderImage;
    public Text loaderText;

    float loadTime = .3f;
    float currTime = 0;

	void Start ()
    {
        // Анимация загрузки
        // Лого
        // Видео
        StartCoroutine (DoLoad());
	}

    IEnumerator DoLoad() // Задержка
    {
        do
        {
            yield return new WaitForEndOfFrame ();
            currTime += Time.deltaTime;
            var time = currTime / loadTime;
            loaderImage.fillAmount = time;
            loaderText.text = "Loading " + ( Mathf.RoundToInt(time  * 100)).ToString() + "%";
            yield return new WaitForEndOfFrame ();
        }while ( currTime < loadTime );

        System.GC.Collect ();
        if ( string.IsNullOrEmpty ( CustomLevelLoader.NextScene ) )
        {
            CustomLevelLoader.NextScene = "menu"; /* Если запустили первый раз
            то переменная будет пустая и мы загружаем Loading
            */
        }
        SceneManager.LoadScene ( CustomLevelLoader.NextScene );
    }
}
