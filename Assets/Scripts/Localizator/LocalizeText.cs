﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizeText : MonoBehaviour
{
    void Awake()
    {
        UnityEngine.UI.Text thisText = GetComponent<UnityEngine.UI.Text>();
        thisText.text = Localizator.GetString ( thisText.text );
    }
}
