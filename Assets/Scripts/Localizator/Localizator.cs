﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public static class Localizator
{
    static JSONNode locJson;
    static string Lang;

    static Localizator()
    {
        // Инициализация
        if(locJson ==null)
        {
            string locString = Resources.Load ( "loc" ).ToString ();
            locJson = JSONNode.Parse ( locString );

            Lang = Application.systemLanguage.ToString();
        }
    }
    public static string GetString(string key)
    {
        return locJson[key][Lang];
    }
}
