﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public static class GameConfigsController
{
    static JSONNode configJson;

    public struct LevelSettings
    {
        public int enemys_hp_value;
        public int level_time_value;
        public int reward_money_value;
    }

    const string game = "game";
	const string levels_settings = "levels_settings";
    const string level = "level";
	const string enemys_hp = "enemys_hp";
    const string level_time = "level_time";
    const string reward_money = "reward_money";

    public static LevelSettings level1;
    public static LevelSettings level2;

    static GameConfigsController()
    {
        // Инициализация
        if ( configJson == null )
        {
            string configString = Resources.Load ( "GameConfigs" ).ToString ();
            configJson = JSONNode.Parse ( configString );
        }
    }

    public static int GetEnemysHp( int level_index )
    {
        level1.enemys_hp_value = configJson[game][levels_settings][level + level_index][enemys_hp].AsInt;
        return level1.enemys_hp_value;
    }

    public static int GetLevelTime( int level_index )
    {
        level1.level_time_value = configJson[game][levels_settings][level + level_index][enemys_hp].AsInt;
        return level1.level_time_value;
    }

    public static int GetRewardMoney( int level_index )
    {
        level1.reward_money_value = configJson[game][levels_settings][level + level_index][enemys_hp].AsInt;
        return level1.reward_money_value;
    }
}
