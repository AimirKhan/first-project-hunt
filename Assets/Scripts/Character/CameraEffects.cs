﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraEffects : MonoBehaviour
{
    public Vector3 angleHitten;

    public Image bloodScreen;

    public static System.Action playerDamaged;

    public Color32 endColor;
    public Color32 normalColor;

    Vector3 normalCameraPosition = new Vector3 ( 0, 0, 0 );
    Vector3 damagedCameraPosition;
    public Transform player;

    void Awake()
    {
        playerDamaged += OnPlayerDamaged;
    }

    void OnDestroy()
    {
        playerDamaged -= OnPlayerDamaged;
    }

    void OnPlayerDamaged()
    {
        // Тут реализуем эффект камеры
        StartCoroutine ( BloodEffectDelay () );
    }

    IEnumerator BloodEffectDelay()
    {
        float time = 0;
        normalCameraPosition = transform.rotation.eulerAngles;
        damagedCameraPosition = normalCameraPosition + new Vector3 ( Random.Range ( -2, 4 ), 0, Random.Range ( -4, 4 ) );
        while (bloodScreen.color.a !=1 && transform.rotation.eulerAngles != damagedCameraPosition)
        {
            yield return new WaitForEndOfFrame ();
            time += Time.deltaTime * 5;
            bloodScreen.color = Color32.Lerp ( bloodScreen.color, endColor, time );
            transform.rotation = Quaternion.Lerp ( transform.rotation, Quaternion.Euler ( damagedCameraPosition ), time );
        }
        bloodScreen.color = normalColor;
        transform.rotation = player.rotation;
    }
}
