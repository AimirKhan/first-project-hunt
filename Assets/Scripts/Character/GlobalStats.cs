﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalStats
{
    public static System.Action refreshMoney;
    public static int Money
    {
        get
        {
            return PlayerPrefsHelper.GetInt ( "Money", 0);
        }
        set
        {
            PlayerPrefsHelper.SetInt ( "Money", value );
            if (refreshMoney != null)
            {
                refreshMoney ();
            }
        }
    }

    public const string super_bullet_effect_string = "SuperBulletEffect";
    public static float SuperBulletEffect
    {
        set
        {
            PlayerPrefsHelper.SetFloat ( super_bullet_effect_string, value);
        }
        get
        {
            return PlayerPrefsHelper.GetFloat ( super_bullet_effect_string, 0 );
        }
    }

    public const string grenade_key_string = "HasGrenade";
    public static bool HasGrenade
    {
        set
        {
            PlayerPrefsHelper.SetBool ( grenade_key_string, value );
        }
        get
        {
            return PlayerPrefsHelper.GetBool ( grenade_key_string, false );
        }
    }
}
