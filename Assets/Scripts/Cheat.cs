﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Cheat
{
    public static System.Action<int> onKillAll;
#if UNITY_EDITOR


    [MenuItem("Custom/Kill All")]
    static void KillAll ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            onKillAll (999999);
        }
	}

    [MenuItem ( "Custom/Get Money 1000" )]
    static void GetMoney()
    {
        GlobalStats.Money += 1000;
    }

    [MenuItem ( "Custom/Clear Prefs" )]
    static void ClearPrefs()
    {
        PlayerPrefs.DeleteAll ();
        PlayerPrefs.Save ();
    }

#endif

}
