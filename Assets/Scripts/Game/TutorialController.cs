﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    public GameObject[] tutorialPhases;

    public static int currentPhase = 0;

    public static System.Action<int> changePhaseAction;

	void Start ()
    {
		if (LevelController.curLevel != 0)
        {
            Destroy ( gameObject );
        }

        ChangePhases ( 0 );

        changePhaseAction += ChangePhases;
	}

    void OnDestroy()
    {
        changePhaseAction -= ChangePhases;
    }

    void ChangePhases(int nextPhase)
    {
        currentPhase = nextPhase;
        for (int i = 0; i < tutorialPhases.Length; i++ )
        {
            tutorialPhases[i].SetActive (i == currentPhase);
        }
    }
}
