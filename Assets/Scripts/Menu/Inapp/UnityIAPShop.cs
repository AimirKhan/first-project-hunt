﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class UnityIAPShop : MonoBehaviour, IStoreListener
{
	public static UnityIAPShop Instance;

	public static event Action<bool> purchaseDone = delegate{};

	private bool m_IsGooglePlayStoreSelected;
	private IStoreController controller;
	private IExtensionProvider extensions;

	#region In-App Purchases ID's

	public static readonly string[] idConsumable = {
		"small_gold_pack",
		"medium_gold_pack",
		"large_gold_pack"
	};

	private readonly string[] idGPConsumable = {
		"medium",
		"large",
		"enormous"
	};

	private readonly string[] idAmazonConsumable = {
		"com.myOrg.appName.small",
		"com.myOrg.appName.medium",
		"com.myOrg.appName.large"
	};

	private readonly string[] idIOSConsumable = {
		"mobi.myOrg.appName.medgp",
		"mobi.myOrg.appName.largegp",
		"mobi.myOrg.appName.enorgp"
	};

	public static readonly string[] idNonConsumable = {
		"no_ads"
	};

	private readonly string[] idGPNonConsumable = {
		"no_ads"
	};

	private readonly string[] idAmazonNonConsumable = {
		"com.myOrg.appName.noads"
	};

	private readonly string[] idIOSNonConsumable = {
		"mobi.myOrg.appName.removeads"
	};

	#endregion

	#region Process Product

	private void ProcessConsumableProduct (int consumableIndex)
	{	
		

		switch (consumableIndex) {
		case 0:
                //награда за покупку
                GlobalStats.Money += 100;
			break;
		case 1:
                //награда за покупку
                GlobalStats.Money += 250;
                break;
		case 2:
                //награда за покупку
                GlobalStats.Money += 1500;
                break;
		}

		purchaseDone (false);
	}

	private void ProcessNonConsumableProduct (int nonConsumableIndex)
	{
		switch (nonConsumableIndex) {
		case 0:
			//награда за покупку

			break;
		}
		purchaseDone (true);


	}

	#endregion

    public bool HasRemoveAds()
    {
        return IsNonConsumablePurchased ( 0 );
    }

	void Awake ()
	{
		m_IsGooglePlayStoreSelected = Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance ().appStore == AppStore.GooglePlay;



		if (Instance == null) {
			Instance = this;
            DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	void Start ()
	{		
		
		InitBiller();
	
	}

	void OnDestroy ()
	{
		PlayerPrefs.Save ();
	}

	public void InitBiller ()
	{
		if (IsInitialized ())
			return;
		
		ConfigurationBuilder builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

		for (int i = 0; i < idConsumable.Length; i++) {
			IDs cids = new IDs ();

			if (idGPConsumable != null && idGPConsumable.Length > i) {
				cids.Add (idGPConsumable [i], GooglePlay.Name);
			}

			if (idIOSConsumable != null && idIOSConsumable.Length > i) {
				cids.Add (idIOSConsumable [i], AppleAppStore.Name);
			}

			if (idAmazonConsumable != null && idAmazonConsumable.Length > i) {
				cids.Add (idAmazonConsumable [i], AmazonApps.Name);
			}

			builder.AddProduct (idConsumable [i], ProductType.Consumable, cids);
		}

		for (int i = 0; i < idNonConsumable.Length; i++) {
			IDs nids = new IDs ();

			if (idGPNonConsumable != null && idGPNonConsumable.Length > i) {
				nids.Add (idGPNonConsumable [i], GooglePlay.Name);
			}	

			if (idIOSNonConsumable != null && idIOSNonConsumable.Length > i) {
				nids.Add (idIOSNonConsumable [i], AppleAppStore.Name);
			}

			if (idAmazonNonConsumable != null && idAmazonNonConsumable.Length > i) {
				nids.Add (idAmazonNonConsumable [i], AmazonApps.Name);
			}

			builder.AddProduct (idNonConsumable [i], ProductType.NonConsumable, nids);
		}

		UnityPurchasing.Initialize (this, builder);
	}

	public void BuyConsumable (int consumableIndex)
	{
        Debug.Log ( "asdd0" );
        if (consumableIndex >= 0 && consumableIndex < idConsumable.Length) {
			BuyProductID (idConsumable [consumableIndex]);
		}
	}

	public void BuyNonConsumable (int nonConsumableIndex)
	{
        Debug.Log ( "asdd0" );
        if (nonConsumableIndex >= 0 && nonConsumableIndex < idNonConsumable.Length) {
			BuyProductID (idNonConsumable [nonConsumableIndex]);
		}
	}

	public void BuyProductID (string productId)
	{		
		try {
			if (IsInitialized ()) {
				Product product = controller.products.WithID (productId);

				if (product != null && product.availableToPurchase) {
					controller.InitiatePurchase (product);
				}
			}
		} catch (Exception e) {			
			Debug.LogError ("BuyProductID: FAIL. Exception during purchase. " + e);
		}
	}

	public void RestorePurchases ()
	{		
		
		if (!IsInitialized ()) {			
			return;
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) {
			extensions.GetExtension<IAppleExtensions> ().RestoreTransactions ((result) => {
				//коллбек для восстановления покупок
			});
		}
	}

	public bool IsInitialized ()
	{
		return controller != null && extensions != null;
	}

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions)
	{
		this.controller = controller;
		this.extensions = extensions;
	}

	public void OnInitializeFailed (InitializationFailureReason error)
	{
	}

	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs e)
	{


		if (e.purchasedProduct.definition.type == ProductType.Consumable) {
			for (int i = 0; i < idConsumable.Length; i++) {
				if (e.purchasedProduct.definition.id.Equals (idConsumable [i])) {
					ProcessConsumableProduct (i);
				}
			}
		} else if (e.purchasedProduct.definition.type == ProductType.NonConsumable) {
			for (int i = 0; i < idNonConsumable.Length; i++) {
				if (e.purchasedProduct.definition.id.Equals (idNonConsumable [i])) {
					PlayerPrefs.SetInt ("IsPurchased_" + idNonConsumable [i], 1);
					ProcessNonConsumableProduct (i);
				}
			}
		}

		StartCoroutine (ClosePurchaseProgressPopup ());
		
		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed (Product i, PurchaseFailureReason p)
	{		
		//что-то пошло не так и покупка отменилась
	}

	#region product metadata getters

	public Product GetConsumableData (int consumableIndex)
	{
		if (controller == null) {
			return null;
		}

		return controller.products.WithID (idConsumable [consumableIndex]);
	}

	public Product GetNonConsumableData (int nonConsumableIndex)
	{
		if (controller == null) {
			return null;
		}
	
		return controller.products.WithID (idNonConsumable [nonConsumableIndex]);
	}

	public bool IsNonConsumablePurchased (int nonConsumableIndex)
	{	
		if (controller != null) {	
			return GetNonConsumableData (nonConsumableIndex).receipt != null || PlayerPrefs.GetInt ("IsPurchased_" + idNonConsumable [nonConsumableIndex],
				0) == 1;
		} else {
			return false;
		}
	}

	#endregion

	IEnumerator ClosePurchaseProgressPopup ()
	{
		var delay = new WaitForEndOfFrame ();

		for (int i = 0; i < 30; i++) {
			yield return delay;
		}
	}
}
