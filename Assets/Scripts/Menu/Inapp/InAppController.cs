﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InAppController : MonoBehaviour
{
    public Text currentMoney;

    [System.Serializable]
    public class ItemSettings
    {
        public string name;
        public Text title;
        public Text description;
        public Button buyButton;
        public Text price;
        public bool consumable;
    }

    public ItemSettings[] allItems;

	void Awake ()
    {
        OnRefreshMoney ();

        for (int i = 0; i < allItems.Length; i++ )
        {
            ItemSettings item;
            item = allItems[i];

            if (item.consumable)
            {
                for ( int j = 0; j < UnityIAPShop.idConsumable.Length; j++ )
                {
                    item.title.text = UnityIAPShop.Instance.GetConsumableData ( j ).metadata.localizedTitle;
                    item.description.text = UnityIAPShop.Instance.GetConsumableData ( j ).metadata.localizedDescription;
                    item.price.text = UnityIAPShop.Instance.GetConsumableData ( j ).metadata.localizedPriceString;
                    int curElement;
                    curElement = j;
                    item.buyButton.onClick.AddListener ( () => UnityIAPShop.Instance.BuyConsumable ( curElement ) );
                }
            }
            else
            {
                for ( int j = 0; j < UnityIAPShop.idNonConsumable.Length; j++ )
                {
                    item.title.text = UnityIAPShop.Instance.GetNonConsumableData ( j ).metadata.localizedTitle;
                    item.description.text = UnityIAPShop.Instance.GetNonConsumableData ( j ).metadata.localizedDescription;
                    item.price.text = UnityIAPShop.Instance.GetNonConsumableData ( j ).metadata.localizedPriceString;
                    item.buyButton.gameObject.SetActive ( UnityIAPShop.Instance.IsNonConsumablePurchased ( j ) );
                    int curElement;
                    curElement = j;
                    item.buyButton.onClick.AddListener ( () => UnityIAPShop.Instance.BuyNonConsumable ( curElement ) );
                }
            }
        }
	}

    void OnRefreshMoney()
    {
        currentMoney.text = GlobalStats.Money.ToString ();
    }
}
