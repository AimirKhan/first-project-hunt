﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DailyBonus : MonoBehaviour
{
    class DaySettings
    {
        public string dayName;
        public Text title;
        public int dayReward;
        public Text dayRewardText;
        public GameObject dayComplete;
        public GameObject currentDayImage;
    }

    public Transform Days;
    DaySettings[] allDays;
    int[] allRewards = { 100, 250, 500, 900, 1500, 2200, 15000 };

    public GameObject dailyBonusPanel;

    int currentRewardDay
    {
        get
        {
            return PlayerPrefsHelper.GetInt ( "currentRewardDay", -1 );
        }
        set
        {
            if(value > 6)
            {
                value = 0;
            }
            PlayerPrefsHelper.SetInt ( "currentRewardDay", value );
        }
    }

    void Awake()
    {
        dailyBonusPanel.SetActive ( false );
        StartCoroutine (CheckCurrentDay ());
        RefreshInfo ();
    }

    IEnumerator CheckCurrentDay()
    {
        DateTime date_now = new DateTime();
       yield return StartCoroutine(AppTime.Instance.GetServerTime (result =>
            {
            if (result)
            {
                date_now = AppTime.CurrentTime;
            }
            else
            {
                // Somethong went wrong
            }
        }));
        DateTime last_day_enter;
        string last_day_string = LoadLastDate();

        if(!DateTime.TryParse( last_day_string, out last_day_enter ) )
        {
            last_day_enter = DateTime.Now;
        }

        TimeSpan lastSessionTime = date_now.Subtract ( last_day_enter ); // тут считаем разницу времени между текущим днём и последней сессией
        if (lastSessionTime.Days ==1 || currentRewardDay == -1)
        {
            // нужно изменить день награды на следующий
            dailyBonusPanel.SetActive ( true );
            currentRewardDay++;
            allDays[currentRewardDay].currentDayImage.SetActive ( true );
        }
        else if (lastSessionTime.Days > 1)
        {
            // нужно обнулить текущий прогресс и дать награду за первый день
            dailyBonusPanel.SetActive ( true );
            currentRewardDay = 0;
            allDays[currentRewardDay].currentDayImage.SetActive ( true );
        }
        else
        {
            dailyBonusPanel.SetActive ( false );
        }
    }

    const string last_day_string_key = "last_day_string";
    void SaveLastDate()
    {
        PlayerPrefsHelper.SetString ( last_day_string_key , DateTime.Now.ToString());
    }

    string LoadLastDate()
    {
        return PlayerPrefsHelper.GetString ( last_day_string_key, DateTime.Now.ToString() );
    }

    void RefreshInfo()
    {
        int curDay = 1;
        allDays = new DaySettings[allRewards.Length];
        for ( int i = 0; i < allRewards.Length; i++ )
        {
            allDays[i] = new DaySettings ();
            allDays[i].title = Days.GetChild ( i ).GetChild ( 0 ).GetComponent<Text> ();
            allDays[i].title.text = "Day " + curDay;
            allDays[i].dayRewardText = Days.GetChild ( i ).GetChild ( 2 ).GetComponent<Text> ();
            allDays[i].dayRewardText.text = allRewards[i].ToString ();
            allDays[i].dayComplete = Days.GetChild ( i ).GetChild ( 1 ).gameObject;
            allDays[i].dayComplete.SetActive ( DayIsComplete ( i ) );
            allDays[i].currentDayImage = Days.GetChild ( i ).GetChild ( 3 ).gameObject;
            allDays[i].currentDayImage.SetActive ( false );
            curDay++;
        }
    }

    const string day_key = "dayComplete";
    bool DayIsComplete(int dayIndex)
    {
        return PlayerPrefsHelper.GetBool ( day_key + dayIndex, false );
    }

    /// <summary>
    /// метод чтобы запомнить, что в этот день мы получили награду
    /// </summary>
    void CurrentDay_GetReward(int dayIndex)
    {
        PlayerPrefsHelper.SetBool ( day_key + dayIndex, true );
    }

    public void OnGetRewardButtonClick()
    {
        if (currentRewardDay < 0)
        {
            currentRewardDay = 0;
        }
        // получаем награду за какой-то день и созраняем его
        SaveLastDate ();
        // запомнили чт ов этот день награда была получена
        CurrentDay_GetReward ( currentRewardDay );
        GlobalStats.Money += allRewards[currentRewardDay];
        dailyBonusPanel.SetActive ( false );
    }
}
