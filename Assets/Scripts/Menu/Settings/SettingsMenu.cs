﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public Slider soundVolumeSlider;

	// Use this for initialization
	void Start ()
    {
        soundVolumeSlider.value = CustomSettings.Volume;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ChangeVolumeSlider()
    {
        CustomSettings.Volume = soundVolumeSlider.value;
    }
}
