﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CustomSettings
{
    private const string sound_volume_Key = "sound_volume";
    public static float Volume
    {
        get
        {
            return PlayerPrefsHelper.GetFloat ( sound_volume_Key, 1 );
        }
        set
        {
            PlayerPrefsHelper.SetFloat ( sound_volume_Key, value );
        }
    }
}
