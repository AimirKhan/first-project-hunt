﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum enItemType
{
    SuperBulletEffect,
    Grenade,
    Deagle,
    none
}

public class MenuShop : MonoBehaviour
{
    public Text currentMoney;

    public ItemSettings[] allItems;

    [System.Serializable]
    public class ItemSettings
    {
        public string name;
        public Text nameText;
        public int price;
        public Text priceText;
        public bool consumable;
        public enItemType itemType = enItemType.none;

        public Button buyButton;
        public Text effectTextPercent;
    }

    void OnBuyItem (int ItemIndex, ItemSettings item)
    {
        if(GlobalStats.Money >= item.price)
        {
            // Купили
            if(item.consumable)
            {
                if(item.itemType == enItemType.SuperBulletEffect )
                {
                    GlobalStats.SuperBulletEffect += 0.5f; // Добавили 0,5% к шансу
                    item.effectTextPercent.text = GlobalStats.SuperBulletEffect.ToString ();
                }
                else
                {
                    Debug.LogError ( "такого эффекта нет" );
                }
            }
            else
            {
                if(item.itemType == enItemType.Grenade)
                {
                    GlobalStats.HasGrenade = true;
                    item.buyButton.gameObject.SetActive ( false );
                }
                else
                {
                    Debug.LogError ( "Такого оружия нету" );
                }
            }
            GlobalStats.Money -= item.price;
        }
        else
        {
            // Переходим в магазин для покупки игровой валюты за реальные деньги
        }
    }

    void InitShop()
    {
        for(int i = 0; i < allItems.Length; i++ )
        {
            var curItem = allItems[i];
            curItem.nameText.text = curItem.name;
            curItem.priceText.text = curItem.price.ToString();
            if ( curItem.consumable )
            {
                if ( curItem.itemType == enItemType.SuperBulletEffect )
                {
                    curItem.effectTextPercent.text = GlobalStats.SuperBulletEffect.ToString() + "%";
                }
            }
            else
            {
                if(curItem.itemType == enItemType.Grenade)
                {
                    curItem.buyButton.gameObject.SetActive ( !GlobalStats.HasGrenade );
                }
            }
            curItem.buyButton.onClick.AddListener (() => OnBuyItem(i, curItem));
        }
    }

	void Awake ()
    {
        InitShop ();
        OnRefreshMoney ();
        GlobalStats.refreshMoney += OnRefreshMoney;
	}
    void OnDestroy()
    {
        GlobalStats.refreshMoney -= OnRefreshMoney;
    }
    void OnRefreshMoney()
    {
        currentMoney.text = GlobalStats.Money.ToString ();
    }
    public void ShowRewardAd()
    {
        AdsController.ShowRewardedAd ();
    }

    public void AddMoney()
    {
        GlobalStats.Money += 1000;
    }
}
