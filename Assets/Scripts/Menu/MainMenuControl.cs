﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuControl : MonoBehaviour
{

    public enum enMenuType
    {
        Normal,
        Settings,
        Shop,
        InApp,
        LevelSelector
    }
    public enMenuType MenuType = enMenuType.Normal;

    public GameObject settingsPanel;
    public GameObject shopPanel;
    public GameObject normalPanel;
    public GameObject inAppPanel;
    public GameObject levelSelector;

    void Refresh()
    {
        switch (MenuType)
        {
            case enMenuType.Normal:
                normalPanel.SetActive ( true );
                shopPanel.SetActive ( false );
                settingsPanel.SetActive ( false );
                inAppPanel.SetActive ( false );
                levelSelector.SetActive ( false );
                break;
            case enMenuType.Settings:
                normalPanel.SetActive ( false );
                shopPanel.SetActive ( false );
                settingsPanel.SetActive ( true );
                inAppPanel.SetActive ( false );
                levelSelector.SetActive ( false );
                break;
            case enMenuType.Shop:
                normalPanel.SetActive ( false );
                shopPanel.SetActive ( true );
                settingsPanel.SetActive ( false );
                inAppPanel.SetActive ( false );
                levelSelector.SetActive ( false );
                break;
            case enMenuType.InApp:
                normalPanel.SetActive ( false );
                shopPanel.SetActive ( false );
                settingsPanel.SetActive ( false );
                inAppPanel.SetActive ( true );
                levelSelector.SetActive ( false );
                break;
            case enMenuType.LevelSelector:
                normalPanel.SetActive ( false );
                shopPanel.SetActive ( false );
                settingsPanel.SetActive ( false );
                inAppPanel.SetActive ( false );
                levelSelector.SetActive ( true );
                break;
            default:
                Debug.LogError ( "Такого состояния нету" ); break;
        }
    }
    
    public void OnInAppPurchases()
    {
        MenuType = enMenuType.InApp;
        Refresh ();
    }
    public void OnShopButtonClick()
    {
        MenuType = enMenuType.Shop;
        Refresh ();
    }

    public void OnSettingsButtonClick()
    {
        MenuType = enMenuType.Settings;
        Refresh ();
    }

    public void OnBackButtonClick()
    {
        MenuType = enMenuType.Normal;
        Refresh ();
    }

    public void OnPlayButtonClick ()
    {
        CustomLevelLoader.LoadLevel ( "game" );
	}
    public void OnLevelSelector()
    {
        MenuType = enMenuType.LevelSelector;
        Refresh ();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            StartCoroutine ( AppTime.Instance.GetServerTime ( result =>
              {
                  if ( result )
                  {
                      Debug.Log ( AppTime.CurrentTime );
                  }
                  else
                  {

                  }
              }
            ) );
        }
    }
}
