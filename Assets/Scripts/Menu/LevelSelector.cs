﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelector : MonoBehaviour
{

    string[] levelsNames =
    {
        "Обучение",
        "Какой-то уровень"
    };
    public class ItemSettings
    {
        public Text titleText;
        public Image mainImage;
        public Text levelScore;
        public Button loadLevelButton;
    }

    ItemSettings[] allLevels;
    public Transform ItemsPanel;

    void Awake()
    {
        allLevels = new ItemSettings[ItemsPanel.childCount];

        for( int i = 0; i < allLevels.Length; i++ )
        {
            int curElement;
            curElement = i;

            allLevels[i] = new ItemSettings ();
            allLevels[i].titleText = ItemsPanel.GetChild ( curElement ).GetChild ( 0 ).GetComponent<Text> ();
            allLevels[i].titleText.text = levelsNames[curElement];

            allLevels[i].levelScore = ItemsPanel.GetChild ( curElement ).GetChild ( 2 ).GetComponent<Text> ();
            allLevels[i].levelScore.text = "345345345/1200"; //TODO: когда появится контроллле для очков, тут нужно переделать

            allLevels[i].mainImage = ItemsPanel.GetChild ( curElement ).GetChild ( 1 ).GetComponent<Image> ();
            allLevels[i].mainImage.sprite = LoadImage( curElement );

            allLevels[i].loadLevelButton = ItemsPanel.GetChild ( curElement ).GetComponent<Button> ();
            allLevels[i].loadLevelButton.onClick.AddListener (() => LoadLevel( curElement ) );
        }
    }
    
    void LoadLevel(int levelIndex)
    {
        LevelController.curLevel = levelIndex;
        CustomLevelLoader.LoadLevel ( "game" );
    }

    Sprite LoadImage(int imageIndex)
    {
        Rect newRect = new Rect (0,0, 1, 1);
        Sprite newSpite = Sprite.Create((Texture2D) Resources.Load ("LevelImages/level" + imageIndex  ), newRect, new Vector2(0,0));
        return (Sprite) Resources.Load ( "LevelImages/level" + imageIndex );
    }
}
