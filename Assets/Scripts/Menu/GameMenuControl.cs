﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenuControl : MonoBehaviour
{
    public GameObject pausePanel;
    public GameObject normalPanel;

    public enum  enMenuStates { normal, pause};

    public enMenuStates menuState = enMenuStates.normal;

    void RefreshStates()
    {
        switch(menuState)
        {
            case enMenuStates.normal:
                Time.timeScale = 1;
                normalPanel.SetActive ( true );
                pausePanel.SetActive ( false );
                break;

            case enMenuStates.pause:
                Time.timeScale = 0;
                normalPanel.SetActive ( false );
                pausePanel.SetActive ( true );
                break;

            default:
                Debug.LogError ( "Default state empty" );
                break;
        }
    }
    public void OnPauseButtonClick()
    {
        menuState = enMenuStates.pause;
        //AdsController.ShowAds ();
        RefreshStates ();
    }
    public void BackButtonClick()
    {
        Time.timeScale = 1;
        menuState = enMenuStates.normal;
        RefreshStates ();
    }
    public void OnMenuButtonClick()
    {
        Time.timeScale = 1;
        CustomLevelLoader.LoadLevel ( "menu" );
    }
    public void OnRestartButtonClick()
    {
        Time.timeScale = 1;
        CustomLevelLoader.LoadLevel ( "game" );
;    }
	void Start ()
    {
        RefreshStates ();
    }
}
