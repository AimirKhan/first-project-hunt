﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SoundPlayer : MonoBehaviour
{
    public static SoundPlayer instance;

    List<AudioClip> sounds = new List<AudioClip>();

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad ( gameObject );

            object[] soundObjects = Resources.LoadAll ( "Sounds/Game" );

            for ( int i = 0; i < soundObjects.Length; i++ )
            {
                sounds.Add ( ( AudioClip ) soundObjects[i] );
            }
        }
        else
        {
            Destroy ( gameObject );
        }
    }

    public void Play(string name, float volume)
    {
        GameObject newSoundObject = new GameObject ( name );
        AudioSource newSoundSource = newSoundObject.AddComponent<AudioSource> ();
        AudioClip clip;

        clip = sounds.First ( testClip => testClip.name == name );
        /*
        for(int i=0; i < sounds.Count; i++ )
        {
            if(name == sounds[i].name)
            {
                clip = sounds[i];
                break;
            }
        }
        */
        newSoundSource.clip = clip;
        newSoundSource.volume = volume * CustomSettings.Volume;
        newSoundSource.Play ();

        Destroy ( newSoundObject, clip.length );
    }
}
