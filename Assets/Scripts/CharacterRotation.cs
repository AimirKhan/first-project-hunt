﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotation : MonoBehaviour
{
    public float mouseSensetivity = 0.3f;

    int minX = -180;
    int maxX = 180;

    int minY = -60;
    int maxY = 60;

    float mouseX;
    float mouseY;

    Vector3 playerRotation;

    float deltaCameraPosition = 0;
    float destinationCameraPosition;
    float deltaMousePosition;

    void Start()
    {
        destinationCameraPosition = Screen.width / 3;
    }

    void Update ()
    {
        if ( Time.timeScale != 0 )
        {
#if UNITY_EDITOR || UNITY_STANDALONE
            mouseX = Mathf.Clamp ( ( Input.mousePosition.x - Screen.width / 2 ) * mouseSensetivity, minX, maxX );
            mouseY = Mathf.Clamp ( ( ( Input.mousePosition.y - Screen.height / 2 ) * -1 ) * mouseSensetivity, minY, maxY );

#else
            if ( Input.touchCount > 0 )
            {
                for ( int i = 0; i < Input.touchCount; i++ )
                {
                    mouseX = Mathf.Clamp ( ( Input.GetTouch ( i ).position.x - Screen.width / 2 ) * mouseSensetivity, minX, maxX );
                    mouseY = Mathf.Clamp ( ( ( Input.GetTouch ( i ).position.y * -1 ) - Screen.height / 2 ) * mouseSensetivity, minY, maxY );
                }
            }
#endif

            if( deltaMousePosition != mouseX)
            {
                deltaCameraPosition += Mathf.Abs (mouseX - deltaMousePosition);
                deltaMousePosition = mouseX;
            }
            
            if(deltaCameraPosition >= destinationCameraPosition)
            {
                if(TutorialController.currentPhase == 0)
                {
                    if(TutorialController.changePhaseAction != null)
                    {
                        TutorialController.changePhaseAction ( 1 );
                    }
                }
            }

            playerRotation.Set ( mouseY, mouseX, 0 );
            transform.eulerAngles = playerRotation;
        }
	}
}
