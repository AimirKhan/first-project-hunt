﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIController : MonoBehaviour
{
    [SerializeField] Text BulletText;

    public static Action<int> OnShoot;

    //Нужен для самой тсрельбы
    public static Action WeaponShoot;

    void Awake()
    {
        OnShoot += BulletRefresh;
    }

    private void OnDestroy()
    {
        OnShoot -= BulletRefresh;
    }

    void BulletRefresh(int bullet)
    {
        BulletText.text = bullet.ToString();
    }

    void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if(Input.GetMouseButtonDown(0))
        {
            if(WeaponShoot != null)
            {
                WeaponShoot ();
            }
        }
#endif
    }

    public void OnshootButtonClick()
    {
        if ( WeaponShoot != null )
        {
            WeaponShoot ();
        }
    }
}
